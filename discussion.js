//CRUD Operation

//Insert Documents (CREATE)
/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"field1": "value1",
				"field2": "value2",
				...
				"fieldN": "valueN",
			});
*/

db.users.insertOne({
	"firstName": "Jane",
	"lasName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

/*
	Syntax:
		Insert Many Documents
			db.collectionName.insertMany([
				{
					"field1": "value1",
					"field2": "value2",
					...
					"fieldN": "valueN",
				},
				{
					"field1": "value1",
					"field2": "value2",
					...
					"fieldN": "valueN",
				},
				...
				{
					"field1": "value1",
					"field2": "value2",
					...
					"fieldN": "valueN",
				}
			]);
*/

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lasName": "Hawking",
		"age": 76,
		"email": "stephenhawking@mail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lasName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@mail.com",
		"department": "none"
	}
]);

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML",
        "isActive": true
	},
	{
		"name": "CSS 101",
        "price": 2500,
        "description": "Introduction to CSS",
        "isActive": false
	}
]);

//Find Documents (READ)
/*
	Syntax:
		Retrieve All Documents
			db.collectionName.find();

		Retrieve Specific Document/s That Match Criteria
			db.collectionName.find({"criteria": "value"});

		Retrive First Document
			db.collectionName.findOne();

		Retrive First Document That Match Criteria
			db.collectionName.findOne({"criteria":"value"});
*/

db.users.find();

db.users.find({"firstName": "Jane"});

db.users.findOne({"department": "none"});

db.users.find({
	"lasName": "Armstrong",
	"age": 82
});

//Update Documents (UPDATE)
/*
	Syntax:
		Update First Document That Match Criteria
			db.collectionName.updateOne(
				{
					"criteria": "field"
				},
				{
					$set: {
						"fieldToUpdate": "newValue"
					}
				}
			);

		Update Many Documents That Match Criteria
			db.collectionName.updateMany(
				{
					"criteria": "field"
				},
				{
					$set: {
						"fieldToUpdate": "newValue"
					}
				}
			);
*/

db.users.insertOne({
	"firstName": "Test",
	"lasName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
});

db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Bill",
			"lasName": "Gates",
			"age": 65,
			"email": "bilgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
);

/*
	Syntax:
		Remove a Field From Document That Match Criteria
			db.collectionName.updateOne(
				{
					"criteria": "field"
				},
				{
					$unset: {
						"fieldToRemove": "value"
					}
				}
			);
*/

db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
);

/*
	Syntax:
		Rename Field
			db.collectionName.updateMany(
				{},
				{
					$rename: {
						"fieldToRename": "newFieldName"
					}
				}
			);
*/

db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}
);

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
);

db.courses.updateMany(
	{},
	{
		$set: {
			"enrollees": 10
		}
	}
);

//Delete Documents (DELETE)

db.users.insertOne({
	"firstName": "Test"
});

/*
	Syntax:
		Delete One Document
			db.collectionName.deleteOne();

		Delete Many Documents
			db.collectionName.deleteMany();
*/

db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"dept": "none"
});